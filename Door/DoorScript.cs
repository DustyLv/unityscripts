﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour 
{

    public float doorCloseAngle = 0f;
    public float doorOpenAngle = 90f;
    float moveSpeed =125f;
    private Quaternion doorOpen = Quaternion.identity;
    private Quaternion doorClose = Quaternion.identity;
    private bool doorStatus = false;
    private bool doorGo = false;

    Vector3 dirDoor;
    float dot;

	void Start () 
	{
        doorOpen = Quaternion.Euler(0, doorOpenAngle, 0);
        doorClose = Quaternion.Euler(0, doorCloseAngle, 0);
        //doorOpen = Quaternion.Euler(doorOpenAngle, 0, 0);
        //doorClose = Quaternion.Euler(doorCloseAngle, 0, 0);
        //doorOpen = Quaternion.Euler(0, 0, doorOpenAngle);
        //doorClose = Quaternion.Euler(0, 0, doorCloseAngle);

	}

    public void DoorAction(Vector3 playerPos)
    {
        dirDoor = transform.position - playerPos;
        dot = Vector3.Dot(dirDoor, transform.right);

        //Debug.Log("Before Coroutine:  dirDoor: " + dirDoor + " , dot: " + dot);
        
        if (dot < 0f)
        {
            doorOpenAngle = 90f;
        }
        else if(dot > 0f)
        {
            doorOpenAngle = -90f;
        }
        Debug.Log("Before Coroutine:  open angle: " + doorOpenAngle);
        doorOpen = Quaternion.Euler(0, doorOpenAngle, 0);

        if (!doorStatus)
        {
            StartCoroutine(this.moveDoor(doorOpen));
        }
        else if (doorStatus)
        {
            StartCoroutine(this.moveDoor(doorClose));
        }


        dirDoor = Vector3.zero;
        dot = 0f;
        Debug.Log("After Coroutine:  open angle: " + doorOpenAngle);
        //Debug.Log("After Coroutine:  dirDoor: " + dirDoor + " , dot: " + dot);
    }

    public IEnumerator moveDoor(Quaternion dest)
    {
        doorGo = true;
        //Check if close/open, if angle less 4 degree, or use another value more 0
        while (Quaternion.Angle(transform.localRotation, dest) > 0.01f)
        {
            //transform.localRotation = Quaternion.Slerp(transform.localRotation, dest, Time.deltaTime * moveSpeed);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, dest, Time.deltaTime * moveSpeed);
            //UPDATE 1: add yield
            yield return null;
        }
        //Change door status
        doorStatus = !doorStatus;
        doorGo = false;

        

        //UPDATE 1: add yield
        yield return null;
    }
}
