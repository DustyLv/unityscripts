﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
	
	public float xSpeed = 0f;
	public float ySpeed = 0f;
	public float zSpeed = 0f;

	void Update () {
		//transform.Rotate(Vector3(0,rotSpeed,0) * Time.deltaTime, Space.Self);
		//transform.Rotate(Vector3.right * Time.deltaTime * rotSpeed);
		transform.Rotate(xSpeed,ySpeed , zSpeed);
	}
}
