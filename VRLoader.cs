﻿using UnityEngine;
using System.Collections;

using UnityEngine.VR;

public class VRLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {

        // If VR device is found, enable VR functionality, else - disable VR globally
        if (VRDevice.isPresent)
        {
            VRSettings.showDeviceView = true; // Show device screen (what user sees) on main display
            VRSettings.enabled = true;
        }
        else
        {
            VRSettings.enabled = false;
        }

        Application.LoadLevel("1_testScene");
	}
}
