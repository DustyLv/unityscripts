﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SceneChangeLoading : MonoBehaviour 
{

	public GameObject background;
	public GameObject text;
	public GameObject progressBar;
	public GameObject canvas;

	private int loadProgress = 0;

	void Start(){
		background.SetActive (false);
		text.SetActive (false);
		progressBar.SetActive (false);
		canvas.SetActive (true);
		AudioListener.volume = 1;
	}

//	public void NextLevelButton(int index)
//	{
//		Application.LoadLevel(index);
//	}
	
	public void NextLevelButton(string levelName)
	{
		StartCoroutine (DisplayLoadingScreen(levelName));
		//Application.LoadLevel(levelName);

	}

	IEnumerator DisplayLoadingScreen(string level){

		background.SetActive (true);
		text.SetActive (true);
		progressBar.SetActive (true);
		canvas.SetActive (false);

		progressBar.transform.localScale = new Vector3 (loadProgress, progressBar.transform.localScale.y, progressBar.transform.localScale.z);

		AsyncOperation async = Application.LoadLevelAsync (level);
		while (!async.isDone) {
			loadProgress = (int)(async.progress * 100);
			progressBar.transform.localScale = new Vector3 (async.progress, progressBar.transform.localScale.y, progressBar.transform.localScale.z);
			yield return null;
		}

	}
	
	//ar UI Extensions progressBar 
	
	    IEnumerator DisplayLoadingScreen(string levelName)
    {
        loadingCanvas.enabled = true;
        AsyncOperation async = Application.LoadLevelAsync(levelName);
        while (!async.isDone)
        {
            int loadProgress = (int)(async.progress * 100);
            loadingSlider.value += loadProgress;
            yield return null;
        }

    }
}