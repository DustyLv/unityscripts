using UnityEngine;
using System.Collections;

public class Singleton : MonoBehaviour {

    //--------------SINGLETON BEGIN--------------//
    static Singleton _instance;

    public static Singleton instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Singleton>();
            }
            return _instance;
        }
    }
    //--------------SINGLETON END--------------//

    void Start () {
	
	}
	
	void Update () {
	
	}
}
