﻿//Place your gun model as a child of the Main Camera in the default FPSWalker. It's position should be x(0.5), y(-0.4), and whatever z position looks best.
//  You may want to adjust the scale of your model to whatever looks best in this position as well.
//Set up an input button named "Sights". You can map this to whatever key or mouse button you want. Mine is set to left shift.
//Attach the following script to the First Person Controller.
//  Drag your gun model (child of the main camera) into the slot for "Gun" on the script.


using UnityEngine;
using System.Collections;

public class AimDownSights : MonoBehaviour {

    public GameObject gunObject;
    public Camera mainCamera;
    public Transform gunAimPos;
    public Transform gunHipPos;
    public string aimButton = "Fire2";

    private float aimedPos;
    private float hipPos;
    private float aimedFOV = 30.0f;
    private float hipFov = 60.0f;

    private Vector3 velocity = Vector3.zero;
    private float smoothTime = 0.1f;

    private float cameraVelocity = 0f;
    private float cameraSmoothTime = 0.1f;

    private bool isAimed = false;

    private float defaultSensitivity;

    private MouseLookPlus mouseLook;
    private MouseLookPlus mouseLookCamera;

	void Start () {
        mouseLook = GetComponent<MouseLookPlus>();
        mouseLookCamera = mainCamera.GetComponent<MouseLookPlus>();
        gunObject.transform.position = gunHipPos.position;

        defaultSensitivity = mouseLook.sensitivity;
	}

	void Update () {

        if (Input.GetButton(aimButton))
        {
            isAimed = true;
            mainCamera.fieldOfView = Mathf.SmoothDamp(mainCamera.fieldOfView, aimedFOV, ref cameraVelocity, cameraSmoothTime);
            gunObject.transform.localPosition = Vector3.SmoothDamp(gunObject.transform.localPosition, gunAimPos.localPosition, ref velocity, smoothTime);


            mouseLook.sensitivity = 4f;
            mouseLookCamera.sensitivity = 4f;

                

        }
        else
        {
            isAimed = false;
            mainCamera.fieldOfView = Mathf.SmoothDamp(mainCamera.fieldOfView, hipFov, ref cameraVelocity, cameraSmoothTime);
            gunObject.transform.localPosition = Vector3.SmoothDamp(gunObject.transform.localPosition, gunHipPos.localPosition, ref velocity, smoothTime);

            mouseLook.sensitivity = defaultSensitivity;
            mouseLookCamera.sensitivity = defaultSensitivity;
        }


	}
}
