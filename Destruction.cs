﻿using UnityEngine;
using System.Collections;

public class Destruction : MonoBehaviour {

    public GameObject remains;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Instantiate(remains, transform.position, transform.rotation);
            Destroy(gameObject);
        }
	}
}
