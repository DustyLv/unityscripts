﻿using UnityEngine;
using System.Collections;

public class ShootInstance : MonoBehaviour {

	public Rigidbody prefab;
	public Transform shootingCube;
	public int throwSpeed = 1000;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			Rigidbody cubeInstance;
			cubeInstance = Instantiate(prefab, shootingCube.position, shootingCube.rotation) as Rigidbody;
			cubeInstance.AddForce(shootingCube.forward * throwSpeed);
		}
	}
}
