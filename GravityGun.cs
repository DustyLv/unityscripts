﻿using UnityEngine;
using System.Collections;

public class GravityGun : MonoBehaviour {

    public float grabDist = 10f;
    public Transform holdPos;
    public float throwForce = 100f;
    public ForceMode throwForceMode;
    public string fireButton = "Fire1";
    public LayerMask layerMask = -1;

    private GameObject heldObj;
    private float journeyLength;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (heldObj == null)
        {
            if (Input.GetButtonDown(fireButton))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, grabDist, layerMask))
                {
                    heldObj = hit.collider.gameObject;
                    heldObj.GetComponent<Rigidbody>().isKinematic = true;
                    heldObj.GetComponent<Collider>().enabled = false;
                }
            }

        }
        else
        {
            //journeyLength = Vector3.Distance(heldObj.transform.position, holdPos.position);

            heldObj.transform.position = Vector3.Lerp(heldObj.transform.position ,holdPos.position, 50f * Time.deltaTime);
            heldObj.transform.rotation = holdPos.rotation;

            if (Input.GetButtonDown(fireButton))
            {
                Rigidbody rb = heldObj.GetComponent<Rigidbody>();
                rb.isKinematic = false;
                heldObj.GetComponent<Collider>().enabled = true;
                rb.AddForce(throwForce * transform.forward, throwForceMode);
                heldObj = null;
            }
        }
	}
}
