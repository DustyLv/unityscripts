﻿using UnityEngine;
using System.Collections;

public class FPSRaycastSpawnObj : MonoBehaviour {

    //public GameObject impactPrefab;
    //GameObject[] impacts;
    //int currentImpact = 0;
    //int maxImpacts = 5;
    bool isShooting = false;
    public GameObject prefab;
    public Transform rayStart;
    public string fireButton = "Use";
      

	void Start () {
        //impacts = new GameObject[maxImpacts];
        //for (int i = 0; i < maxImpacts; i++)
        //{
        //    impacts[i] = (GameObject)Instantiate(impactPrefab, new Vector3(0, 1000, 0), Quaternion.identity);
        //}
	}

	void Update () {
        if (Input.GetButtonDown(fireButton))
        {
            isShooting = true;
        }
	}

    void FixedUpdate()
    {
        if (isShooting == true)
        {
            isShooting = false;

            RaycastHit hit;
            if (Physics.Raycast(rayStart.position, transform.forward, out hit, 100f))
            {
                //impacts[currentImpact].transform.position = hit.point;
                //impacts[currentImpact].GetComponent<ParticleSystem>().Play();
                //if (++currentImpact >= maxImpacts)
                //{
                //    currentImpact = 0;
                //}
                Instantiate(prefab, new Vector3(hit.point.x, hit.point.y+2, hit.point.z), Quaternion.identity);
                print(hit.point);
            }
        }
    }
}
