﻿using UnityEngine;
using System.Collections;

public class FPSCameraZoom : MonoBehaviour {

	public int zoom = 35;
	public int normal = 60;
	public float smooth = 2f;

	void Update () {
		if (Input.GetButton ("Fire2") == true) {
			GetComponent<Camera> ().fieldOfView = Mathf.Lerp (GetComponent<Camera> ().fieldOfView, zoom, Time.deltaTime * smooth);
		} else {
			GetComponent<Camera> ().fieldOfView = Mathf.Lerp (GetComponent<Camera> ().fieldOfView, normal, Time.deltaTime * smooth);
		}
	}
}
