﻿using UnityEngine;
using System.Collections;

// This script is used with DoorScript.cs on a door to open

public class PlayerScript : MonoBehaviour 
{
    RaycastHit hit;

	void FixedUpdate () 
	{
        if (Input.GetKeyDown(KeyCode.E))
        {
            DoorRaycast();
        }
	}

    void DoorRaycast()
    {
           if (Physics.Raycast(transform.position, transform.forward, out hit, 5f))
           {
                if(hit.collider.gameObject.tag == "Door")
                {
                    //hit.collider.SendMessageUpwards ("DoorAction", transform.position);
                    //hit.collider.BroadcastMessage("DoorAction", transform.position);
                    hit.collider.gameObject.GetComponent<DoorScript>().DoorAction(transform.position);
                hit.collider.gameObject.GetComponent<DoorScript>().doorOpenAngle = 90f;
                }
           }
        //, SendMessageOptions.DontRequireReceiver

    }
}
