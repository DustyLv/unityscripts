﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AIFollowScript : MonoBehaviour {

    GameObject player;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");

        if (!player)
        {
            Debug.Log("Make sure your player is tagged!!");
        }
	}
    void Update()
    {
        Vector3 fleeDirection = (player.transform.position - transform.position).normalized;
        float fleeingDistance = 20;

        if (!PlayerScript.isSuper)
        {
            GetComponent<NavMeshAgent>().destination = player.transform.position;
            GetComponent<NavMeshAgent>().speed = 3.5f;
        }
        else
        {
            GetComponent<NavMeshAgent>().destination = transform.position + fleeDirection * fleeingDistance;
            GetComponent<NavMeshAgent>().speed = 1f;
        }
    }
}
