﻿using UnityEngine;
using System.Collections;

public class ThirdPersonWalking : MonoBehaviour {

	private Animator anim;
	private float vert;
	private float horiz;
	private bool sprint = false;
	public float sprintSpeed = 7.0f;
	public float walkSpeed = 5.0f;
	public float gravity = 10.0f;
	public float jumpSpeed = 5.0f;
	private float speed;
	public float turnSpeed = 60.0f;
	private bool grounded = true;
	private bool jump = false;
	private Vector3 moveDirection = Vector3.zero;

//	public AudioClip walkSound;
//	private AudioSource source;
//	private float volLowRange = .5f;
//	private float volHighRange = 1.0f;
//	private float delay = 1.0f;

	void Awake () {
	
		anim = GetComponent<Animator> ();
		//source = GetComponent<AudioSource>();
	}

	void Update () {

		if (Input.GetButton ("Sprint")) {
			sprint = true;
		} else {
			sprint = false;
		}

		if (Input.GetButton ("Jump")) {
			jump = true;
		} else {
			jump = false;
		}
	////////////fiziskā kustība
		CharacterController controller = GetComponent<CharacterController>();
		float turn = Input.GetAxis ("Horizontal");
		transform.Rotate (0,turn * turnSpeed * Time.deltaTime,0);
		if (controller.isGrounded) {
			if(Input.GetButton("Sprint")){
				speed = sprintSpeed;
			}else{
				speed = walkSpeed;
			}
			moveDirection = transform.forward * Input.GetAxis ("Vertical") * speed;
			if (Input.GetButton ("Jump"))
				moveDirection.y = jumpSpeed;

		} else {
		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);
		////////

//		if (Input.GetAxis ("Vertical") != 0) {
//			source.PlayDelayed(delay);
//		}

		vert = Input.GetAxis ("Vertical");

		anim.SetFloat ("walk", vert);
		anim.SetBool ("isRun", sprint);
		anim.SetBool ("grounded", grounded);
		anim.SetBool ("jump", jump);
	}
}
