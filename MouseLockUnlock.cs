﻿using UnityEngine;
using System.Collections;

public class MouseLockUnlock : MonoBehaviour {

	private bool showingMouse;

	void Awake () {
		showingMouse = false;
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	void Update() {
		//if you press "r"  and the mouse is not shown then the mouse is shown and movable then execute code
		if(Input.GetKeyDown(KeyCode.R) && showingMouse == false){
			showingMouse = true;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
		//if you press "r" and the mouse is shown then make it not shown
		else if(Input.GetKeyDown(KeyCode.R) && showingMouse == true){
			showingMouse = false;
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
	}

}
}
