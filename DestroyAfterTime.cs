﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

	public float timeToLive = 2f;
	
	// Use this for initialization
	void Start () {
		Destroy (gameObject, timeToLive);
	
	}
}
