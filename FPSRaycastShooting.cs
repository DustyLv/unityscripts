﻿using UnityEngine;
using System.Collections;

public class FPSRaycastShooting : MonoBehaviour {

    public GameObject impactPrefab;
    GameObject[] impacts;
    int currentImpact = 0;
    int maxImpacts = 5;
    bool isShooting = false;

	void Start () {
        impacts = new GameObject[maxImpacts];
        for (int i = 0; i < maxImpacts; i++)
        {
            impacts[i] = (GameObject)Instantiate(impactPrefab, new Vector3(0, 1000, 0), Quaternion.identity);
        }
	}

	void Update () {
        if (Input.GetButton("Fire1"))
        {
            isShooting = true;
        }
	}

    void FixedUpdate()
    {
        if (isShooting == true)
        {
            isShooting = false;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 30f))
            {
                impacts[currentImpact].transform.position = hit.point;
                impacts[currentImpact].GetComponent<ParticleSystem>().Play();
                if (++currentImpact >= maxImpacts)
                {
                    currentImpact = 0;
                }
            }
        }
    }
}
