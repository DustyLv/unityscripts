﻿//Attach to a camera

using UnityEngine;
using System.Collections;

public class AIWalkCommand : MonoBehaviour {

    Camera camera;
    bool isShooting = false;
    public GameObject AIObject;
    public static Vector3 moveTarget;

	void Start () {
        camera = GetComponent<Camera>();
        moveTarget = Vector3.zero;
	}

	void Update () {
        if (Input.GetButtonDown("Use"))
        {
            isShooting = true;
        }
        AIObject.GetComponent<NavMeshAgent>().destination = moveTarget;
        AIObject.GetComponent<NavMeshAgent>().speed = 3.5f;
    }

    void FixedUpdate()
    {
        if (isShooting)
        {
            isShooting = false;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray,out hit,200f))
            {
                moveTarget = hit.point;
            }
        }
    }
}
